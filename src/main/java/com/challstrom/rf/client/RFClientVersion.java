package com.challstrom.rf.client;

public class RFClientVersion {
    private static final int MAJOR = 0;
    private static final int MINOR = 1;
    private static final int PATCH = 0;

    public static String getVersionString() {
        return MAJOR + "." + MINOR + "." + PATCH;
    }
}
