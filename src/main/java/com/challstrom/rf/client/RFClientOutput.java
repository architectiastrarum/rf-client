package com.challstrom.rf.client;

import com.challstrom.rf.client.gui.RFGClientController;
import com.challstrom.rf.core.RFSec;
import com.challstrom.rf.core.packet.RFPacket;
import com.challstrom.rf.core.packet.RFPacketBuffer;
import com.challstrom.rf.core.packet.RFVerbose;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.CRC32;

public class RFClientOutput implements Runnable {
    private final RFPacketBuffer packetBuffer;
    private final String passphrase;
    private final Set<String> sendWait;
    private static final ArrayList<String> decryptedList = new ArrayList<>();
    private static final HashSet<String> clientList = new HashSet<>();
    public static RFGClientController rfgClientController;

    RFClientOutput(RFPacketBuffer packetBuffer, String passphrase, Set<String> sendWait) {
        this.packetBuffer = packetBuffer;
        this.passphrase = passphrase;
        this.sendWait = sendWait;
    }


    @Override
    public void run() {
        Pattern harmonizationPattern = Pattern.compile("\\*\\*(.*) harmonized\\*\\*");
        Pattern deHarmonizationPattern = Pattern.compile("\\*\\*(.*) de-harmonized\\*\\*");
        SecretKey secretKey = null;
        //MAKE SURE THIS GETS RESET AFTER A RUN
        CRC32 crc32 = new CRC32();
        try {
            secretKey = RFSec.getSecret(passphrase);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            e.printStackTrace();
        }
        while (Main.systemIsRunning.get()) {
            if (packetBuffer.hasNext()) {
                RFPacket receivedPacket = packetBuffer.removeNextPacket();
                String decrypted = "";
                try {
                    decrypted = RFSec.decrypt(receivedPacket.getEncodedMessage(), receivedPacket.getIv(), secretKey);
                } catch (NoSuchPaddingException | NoSuchAlgorithmException | BadPaddingException | UnsupportedEncodingException | IllegalBlockSizeException | InvalidAlgorithmParameterException | InvalidKeyException ignored) {
                    //decryption failed
                    if (RFVerbose.useVerbose) {
                        System.out.println("Packet received, but decryption failed. Skipping Packet...");
                    }
                }
                crc32.update(decrypted.getBytes());
                long crcPredicted = crc32.getValue();
                crc32.reset();
                long crcObserved = receivedPacket.getCrc();
                if (crcPredicted == crcObserved) {
                    synchronized (this) {
                        if (!decryptedList.contains(decrypted)) decryptedList.add(decrypted);
                        rfgClientController.updateOutput(decryptedList);
                        if (sendWait.contains(decrypted)) {
                            sendWait.remove(decrypted);
                            System.out.println("[" + receivedPacket.getCrc() + " was broadcast]");
                        } else {
                            Matcher harmonizationMatcher = harmonizationPattern.matcher(decrypted);
                            Matcher deHarmonizationMatcher = deHarmonizationPattern.matcher(decrypted);
                            if (harmonizationMatcher.find()) {
                                clientList.add(harmonizationMatcher.group(1));
                                rfgClientController.updateClients(clientList);
                            } else if (deHarmonizationMatcher.find()) {
                                clientList.remove(deHarmonizationMatcher.group(1));
                                rfgClientController.updateClients(clientList);
                            }
                            System.out.println(decrypted);
                        }
                    }
                }
            }
        }
    }
}
