package com.challstrom.rf.client;


import com.challstrom.rf.client.gui.RFGClient;
import com.challstrom.rf.core.packet.RFPacket;
import com.challstrom.rf.core.packet.RFPacketFactory;
import com.challstrom.rf.core.packet.RFVerbose;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.security.GeneralSecurityException;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.IntStream;

public class Main {
    static final AtomicBoolean systemIsRunning = new AtomicBoolean(true);
    //    public static RFClientOutput rfClientOutput;
    public static RFClientConnection clientConnection;
    static final RFPacketFactory factory = new RFPacketFactory();
    static RFGClient rfgClient = new RFGClient();
    //    static RFPacketBuffer outboundBuffer = new RFPacketBuffer();
//    static String nickname;
    static Set<String> sendWait = new HashSet<>();
    //    private static String password;
    static String[] args;

    public static void main(String[] args) {
        Main.args = args;
        System.out.println("=====STARTING RADIANT CLIENT=====");
        RFVerbose.useVerbose = false;
        rfgClient.makeGUI(args);
//        Scanner scanner = new Scanner(System.in);
//        System.out.print("Please enter an server address to subscribe to: ");
//        String serverAddress = scanner.nextLine();
//        System.out.print("Please enter a port to listen on: ");
//        int clientPort = scanner.nextInt();
//        scanner.nextLine();
//        int clientPort = findNextAvailablePort();
//        System.out.println("Client binding to port " + clientPort);
//        System.out.println("Please enter a nickname (optional): ");
//        nickname = scanner.nextLine();
//        nickname = Objects.equals(nickname, "") ? "Anonymous" : nickname;
//        System.out.print("Please enter a passphrase to encrypt with: ");
//        password = scanner.nextLine();
//        RFRoute rfServerRoute = null;
//        try {
//            rfServerRoute = new RFRoute(InetAddress.getByName(serverAddress), 4157);
//        } catch (UnknownHostException e) {
//            e.printStackTrace();
//        }
//
//        DatagramSocket packetSocket = null;
//        try {
//            packetSocket = new DatagramSocket(clientPort);
//        } catch (SocketException e) {
//            e.printStackTrace();
//        }
//
//        //setup  receiving
//        RFPacketBuffer inboundBuffer = new RFPacketBuffer();
//        RFCommandHandler commandHandler = new ClientCommandHandler();
//        RFPacketReceiver packetReceiver = new RFPacketReceiver(inboundBuffer, packetSocket, systemIsRunning, commandHandler);
//
//        rfClientOutput = new RFClientOutput(inboundBuffer, password, sendWait);
//
//        //setup sending
//
//        ConcurrentLinkedQueue<RFRoute> rfRoute = new ConcurrentLinkedQueue<>();
//        rfRoute.add(rfServerRoute);
//        RFPacketSender packetSender = new RFPacketSender(outboundBuffer, packetSocket, systemIsRunning, 1000, rfRoute);


        //since we're already running, ignore stderr unless verbose is active
        if (!RFVerbose.useVerbose) {
            System.setErr(new PrintStream(new OutputStream() {
                public void write(int b) {
                }
            }));
        }

//        while (systemIsRunning.get()) {
//            synchronized (factory) {
//                System.out.println("Please enter a message to send: ");
//            }
//            String message = scanner.nextLine();
//            if (Objects.equals(message.toUpperCase(), "!STOP")) {
//                stopClient();
//            } else sendMessage(message);
//        }
    }


    public static void stopClient() {
        if (!clientConnection.isStarted()) return;
        try {
            RFPacket announcePacket = factory.getDataPacket("**(" + clientConnection.getNickname() + ") de-harmonized**", clientConnection.getPassword());
            clientConnection.addPacketToOutboundBuffer(announcePacket);
        } catch (GeneralSecurityException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        RFPacket unSubscribeCommand = factory.getCommand("!UNSUBSCRIBE");
        clientConnection.addPacketToOutboundBuffer(unSubscribeCommand);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        systemIsRunning.set(false);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("=====RADIANT CLIENT STOPPED=====");
        System.exit(0);
    }



    private static boolean portAvailable(int port) {
        ServerSocket ss = null;
        DatagramSocket ds = null;
        try {
            ss = new ServerSocket(port);
            ss.setReuseAddress(true);
            ds = new DatagramSocket(port);
            ds.setReuseAddress(true);
            return true;
        } catch (IOException ignored) {
        } finally {
            if (ds != null) {
                ds.close();
            }

            if (ss != null) {
                try {
                    ss.close();
                } catch (IOException ignored) {
                }
            }
        }
        return false;
    }

    public static int findNextAvailablePort() {
        ConcurrentLinkedQueue<Integer> portsAvailable = new ConcurrentLinkedQueue<>();
        int min = 1000;
        int max = 1005;
        while (portsAvailable.size() < 1) {
            IntStream.range(min, max).parallel().forEach(i -> {
                if (portAvailable(i)) portsAvailable.add(i);
            });
            min = max + 1;
            max += 10;
        }
        return portsAvailable.remove();
    }
}
