package com.challstrom.rf.client;

import com.challstrom.rf.client.gui.RFGClient;
import com.challstrom.rf.core.RFCommandHandler;
import com.challstrom.rf.core.RFRoute;
import com.challstrom.rf.core.packet.*;

import java.io.UnsupportedEncodingException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class RFClientConnection {
    private final RFPacketFactory factory = new RFPacketFactory();
    private RFGClient rfgClient = new RFGClient();
    private RFPacketBuffer outboundBuffer = new RFPacketBuffer();
    private RFPacketBuffer inboundBuffer = new RFPacketBuffer();
    private Set<String> sendWait = new HashSet<>();
    private boolean initialized = false;
    private boolean preInitialized = false;
    private boolean started = false;
    private RFRoute rfServerRoute;
    private RFPacketReceiver packetReceiver;
    private RFPacketSender packetSender;
    private String nickname;
    private String password;
    private DatagramSocket packetSocket;

    private static String[] convertToBlocks(String inputString) {
        int size = (inputString.length() / 842) + (inputString.length() % 842 > 0 ? 1 : 0);
        String[] output = new String[size];
        for (int i = 0; i < size; i++) {
            output[i] = inputString.substring((i * 842), (i + 1) * 842 < inputString.length() ? (i + 1) * 842 : inputString.length());
        }
        return output;
    }

    public boolean preInitialize(int clientPort) {
        packetSocket = null;
        try {
            packetSocket = new DatagramSocket(clientPort);
        } catch (SocketException e) {
            e.printStackTrace();
            return false;
        }
        preInitialized = true;
        return true;
    }

    public void initialize(String serverAddress) {
        initialized = false;
        try {
            rfServerRoute = new RFRoute(InetAddress.getByName(serverAddress), 4157);
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return;
        }

        //setup  receiving
        RFCommandHandler commandHandler = new ClientCommandHandler();
        packetReceiver = new RFPacketReceiver(inboundBuffer, packetSocket, Main.systemIsRunning, commandHandler);

        //setup sending

        ConcurrentLinkedQueue<RFRoute> rfRoute = new ConcurrentLinkedQueue<>();
        rfRoute.add(rfServerRoute);
        packetSender = new RFPacketSender(outboundBuffer, packetSocket, Main.systemIsRunning, 1000, rfRoute);
        initialized = true;
    }

    public void close() {
        packetSocket.close();
    }

    public RFPacketFactory getFactory() {
        return factory;
    }

    public RFGClient getRfgClient() {
        return rfgClient;
    }

    public RFPacketBuffer getOutboundBuffer() {
        return outboundBuffer;
    }

    public RFPacketBuffer getInboundBuffer() {
        return inboundBuffer;
    }

    public Set<String> getSendWait() {
        return sendWait;
    }

    public boolean isInitialized() {
        return initialized;
    }

    public boolean isPreInitialized() {
        return preInitialized;
    }

    public boolean isStarted() {
        return started;
    }

    public RFRoute getRfServerRoute() {
        return rfServerRoute;
    }

    public RFPacketReceiver getPacketReceiver() {
        return packetReceiver;
    }

    public RFPacketSender getPacketSender() {
        return packetSender;
    }

    public String getNickname() {
        return nickname;
    }

    public String getPassword() {
        return password;
    }

    public void start(String nickname, String password) {
        this.nickname = nickname;
        this.password = password;

        RFClientOutput rfClientOutput = new RFClientOutput(inboundBuffer, password, sendWait);

        //setup thread pool, submit threads
        ExecutorService executor = Executors.newCachedThreadPool();
        executor.submit(packetReceiver);
        executor.submit(packetSender);
        executor.submit(rfClientOutput);
        executor.submit(() -> rfgClient.makeGUI(Main.args));


        //subscribe and send harmonization notification
        RFPacket subscribeCommand = factory.getCommand("!SUBSCRIBE");
        outboundBuffer.addPacket(subscribeCommand);
        try {
            RFPacket announcePacket = factory.getDataPacket("**(" + nickname + ") harmonized**", password);
            outboundBuffer.addPacket(announcePacket);
        } catch (GeneralSecurityException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        started = true;
    }

    public void sendMessage(String message) {
        {
            message = "(" + nickname + ") " + message;
            String[] messages = convertToBlocks(message);
            Arrays.stream(messages).forEach(m -> {
                sendWait.add(m);
                try {
                    RFPacket packet = factory.getDataPacket(m, password);
                    System.out.println("\"" + m + "\" queued as number " + packet.getCrc());
                    outboundBuffer.addPacket(packet);
                } catch (GeneralSecurityException | UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            });
        }
    }

    public void addPacketToOutboundBuffer(RFPacket rfPacket) {
        outboundBuffer.addPacket(rfPacket);
    }
}
