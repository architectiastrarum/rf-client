package com.challstrom.rf.client.gui;

import com.challstrom.rf.client.Main;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.net.URL;

public class RFGClient extends Application {

    public void makeGUI(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        String resourcePath = "rfEntryScreen.fxml";
        URL location = getClass().getClassLoader().getResource(resourcePath);
        FXMLLoader fxmlLoader = new FXMLLoader(location);
        Scene scene = new Scene(fxmlLoader.load(), 512, 768);

        primaryStage.setTitle("RF Client");
        primaryStage.setScene(scene);
        primaryStage.setOnCloseRequest(event -> Main.stopClient());
        primaryStage.show();
    }
}
