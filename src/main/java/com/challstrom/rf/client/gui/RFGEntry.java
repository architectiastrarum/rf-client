package com.challstrom.rf.client.gui;

import com.challstrom.rf.client.Main;
import com.challstrom.rf.client.RFClientConnection;
import com.challstrom.rf.client.RFClientVersion;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.challstrom.rf.client.Main.clientConnection;

public class RFGEntry implements Initializable {

    @FXML
    public Label versionNumber;

    @FXML
    private ProgressBar connectionProgress;

    @FXML
    private TextField serverAddress;

    @FXML
    private TextField nickname;

    @FXML
    private PasswordField password;

    @FXML
    private Button connectButton;

    private String currentAddress;
    private Pattern pattern;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        versionNumber.setText("Version " + RFClientVersion.getVersionString());
        int clientPort = Main.findNextAvailablePort();
        connectionProgress.setProgress(0.25);
        serverAddress.focusedProperty().addListener((arg0, oldPropertyValue, newPropertyValue) -> {
            if (!newPropertyValue) {
                if (checkAddress()) {
                    if (clientConnection != null && clientConnection.isPreInitialized()) {
                        clientConnection.close();
                    }
                    clientConnection = new RFClientConnection();
                    if (clientConnection.preInitialize(clientPort)) {
                        connectionProgress.setProgress(0.50);
                        clientConnection.initialize(serverAddress.getText());
                        connectButton.setDisable(false);
                    } else {
                        connectionProgress.setProgress(0.25);
                        serverAddress.setPromptText("Could not connect to " + serverAddress.getText());
                        serverAddress.setText("");
                    }
                }
            }
        });
    }

    public void connect(ActionEvent actionEvent) {
        //do connection stuff
        if (clientConnection.isInitialized()) {
            connectionProgress.setProgress(0.75);
            clientConnection.start(nickname.getText(), password.getText());
            connectionProgress.setProgress(1.0);
            String resourcePath = "rfGClient.fxml";
            URL location = getClass().getClassLoader().getResource(resourcePath);
            FXMLLoader fxmlLoader = new FXMLLoader(location);
            Scene scene = null;
            try {
                scene = new Scene(fxmlLoader.load(), 512, 768);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Stage secondStage = new Stage();
            secondStage.setTitle("RF Client");
            secondStage.setScene(scene);
            secondStage.setOnCloseRequest(event -> Main.stopClient());
            System.out.println("Showing stage 2.");
            ((Node) (actionEvent.getSource())).getScene().getWindow().hide();
            secondStage.show();
        }

    }

    public boolean checkAddress() {
        if (pattern == null) {
            pattern = Pattern.compile("(^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\\-]*[A-Za-z0-9])$) |(^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\\-]*[A-Za-z0-9])$)");
        }
        String enteredAddress = serverAddress.getText();
        Matcher matcher = pattern.matcher(enteredAddress);
        return matcher.matches();
    }
}
