package com.challstrom.rf.client.gui;

import com.challstrom.rf.client.Main;
import com.challstrom.rf.client.RFClientOutput;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

/**
 * Created by TJ Challstrom on 28-Jan-18 at 04:57 PM.
 * Dispenser goes here!
 */
public class RFGClientController implements Initializable {

    protected ListProperty<String> outputListProperty = new SimpleListProperty<>();
    protected ListProperty<String> clientListProperty = new SimpleListProperty<>();
    @FXML
    private TextField messageField;
    @FXML
    private Button sendButton;
    @FXML
    private ListView clientListView;
    @FXML
    private ListView outputListView;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        outputListView.itemsProperty().bind(outputListProperty);
        clientListView.itemsProperty().bind(clientListProperty);

        RFClientOutput.rfgClientController = this;

        outputListProperty.set(FXCollections.observableArrayList(new ArrayList<>()));
        clientListProperty.set(FXCollections.observableArrayList(new ArrayList<>()));

        sendButton.setOnAction(handler -> {
            Main.clientConnection.sendMessage(messageField.getText());
            messageField.setText("");
        });
    }

    public void updateOutput(List<String> newList) {
        outputListProperty.set(FXCollections.observableArrayList(newList));
    }

    public void updateClients(Set<String> newSet) {
        clientListProperty.set(FXCollections.observableArrayList(newSet));
    }

    public void onEnter(ActionEvent actionEvent) {
        Main.clientConnection.sendMessage(messageField.getText());
        messageField.setText("");
    }
}
